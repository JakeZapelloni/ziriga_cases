jQuery(document).ready(function($){
	//open-close submenu on mobile
	$('.cd-main-nav').on('click', function(event){
		if($(event.target).is('.cd-main-nav')) $(this).children('ul').toggleClass('is-visible');
	});


    // $('div.bgParallax').each(function(){
    //     var $obj = $(this);
        
    //     $(window).scroll(function() {
    //         var xPos = -($(window).scrollTop() / $obj.data('speed')); 
     
    //         var bgpos = '50% '+ xPos + 'px';
     
    //         $obj.css('background-position', bgpos );
     
    //     }); 
    // });
});


function enviarContato(caminho,dados,msgconfig){
        $.ajax({
            url:caminho,
            type:"POST",
            data:dados,
            dataType:'json',
            beforeSend: function(){
                msgconfig.status.addClass('hidden');
            },
            success: function(retorno){
                if(retorno.status){
                    msgconfig.enviado.removeClass("hidden");
                }else{
                    msgconfig.status.addClass('hidden');
                    msgconfig.problema.removeClass('hidden');
                }
            },
            error: function(){
                msgconfig.problema.removeClass('hidden');
            }
        })
    }

    function validar_campo($campo){
        if($campo.val() == ""){
            $campo.addClass("form-error");
            $campo.parent().find(".help-block").addClass("valid-error");
            return false;
        }
        $campo.removeClass("form-error");
        $campo.parent().find(".help-block").removeClass("valid-error");
        return true;
    }

    /* form validation  formulario visita-banner*/
    $('#contactForm').submit(function(evento){
        evento.preventDefault();

        var situacao_do_form = true;

        var nome = $('#contactName');
        var email = $('#contactEmail');
        var telefone = $('#telefone');

        situacao_do_form = validar_campo(nome);
        situacao_do_form = validar_campo(email);
        situacao_do_form = validar_campo(telefone);

        var msconfig = {
            status: $('.form-status'),
            enviado: $('.form-enviado'),
            problema: $('.form-problema')
        }


        if(situacao_do_form){
            enviarContato('mail.php',$(this).serialize(),msconfig);
        }

    })



